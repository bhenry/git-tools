#!/usr/bin/env ruby

#arbitrary command against /apps git dirs

#TODO use root as default, but allow for list of files (allow bash wildcard expansion to list, or free entry of multiple entries)

root = Dir.pwd #'/apps'
ignore=[]
puts "ignoring dirs in #{ignore}"

def each_git_dir(root, ignore=[], &block)
  Dir.foreach(root) do |name|
    next if name =~ /^\..*/  #filter ., .., and secret dirs/files
    path = File.join(root, name)
    next if !Dir.exist?(path) #filter files out
    next if !Dir.exist?(File.join(path, '.git')) #get gitable's
    next if ignore.include?(name)  #already know this is dir, by virtue of geting this far

    yield path
  end
end

each_git_dir(root) do |path|
  puts path
  Dir.chdir(path) #change wd
  
  puts `#{ARGV[0]}`
  puts ''
end
