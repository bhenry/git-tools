#!/usr/bin/env ruby

#RUN THIS FROM TARGET DIR.  this will check all repos for org and if any are not present, clones them.

# TODO add in check (or use separate script) to see if present git project is not on GH anymore.  allow for delete/archive elsewhere.

# this prolly supercedes the grab_bulk_git_repos script i wrote.  no need to feed it a list

# it would be jim-damdy if this were parallelized.  never tried that with MRI and gil and such, and i forget the limitations.
#note this could conceivably do something about repos that don't exist anymore.  rename?  move?  or maybe nothing is fine.  flag and require manual action to change...

#might not be terrible to do a (safe?) pull to refresh if we already have the repo.  not sure if that could mess things up.  --ff-only?  if you're on a headless branch - any impact?  maybe just do a fetch?

#`gem install octokit` before use.  in my case, `rvm gemset use git_Stuff`...
require 'octokit'

#token needs 'repo' and it's subperms
access_token = ENV['GIT_ACCESS_TOKEN'] #todo - move this out to gitignored file

org_name = ENV['GIT_ORG_NAME']


#repos_root = File.dirname(__FILE__)  meh - run from target dir. otherwise, figure out how to cwd before cloning.

#TODO
#accept an arg with a regex for repos to get?  

#TODO
#(external) blacklist?
#   base = File.dirname(__FILE__)
#   File.open(base + '/repo_blacklist.txt').each_line do |ln|
#     #populate blacklist to check against in below loop
#   end



client = Octokit::Client.new(access_token: access_token)
client.auto_paginate = true

repos = client.organization_repositories(org_name).sort do |a,b| 
  a.name <=> b.name 
end

repos.each do |repo|
  name = repo.name
  
  if !File.exist? repo.name
    puts "* cloning #{name}"
    `git clone #{repo.ssh_url}`
  else 
    puts "- already have #{repo.name}.  skipping." 
  end  
end
